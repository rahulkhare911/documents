/**********************************************************************************
 *  TITLE: Manual (Switch - Latching Switch) + Alexa + IR Remote control 8-channel Relay Module using ESP32
 *  Click on the following links to learn more. 
 *  YouTube Video: https://youtu.be/5-ZyC3-BzLE
 *  Related Blog : https://iotcircuithub.com/esp32-projects/
 *  by Subhajit (Tech StudyCell)
 *  
 *  Preferences--> Aditional boards Manager URLs : 
 *  https://dl.espressif.com/dl/package_esp32_index.json, http://arduino.esp8266.com/stable/package_esp8266com_index.json
 *  
 *  Download Board:
 *  For ESP32 : https://github.com/espressif/arduino-esp32
 *  
 *  Download the libraries:
 *  Espalexa Library: https://github.com/Aircoookie/Espalexa
 *  AceButton Library: https://github.com/bxparks/AceButton
 *  IRremote Library: https://github.com/Arduino-IRremote/Arduino-IRremote
 **********************************************************************************/

#include <WiFi.h>
#include <Espalexa.h>
#include <IRremote.h>
#include <Servo.h> // servo library  

Servo s1;

Espalexa espalexa;

// define the GPIO connected with Relays and switches
#define RelayPin1 23  //D23
#define RelayPin2 22  //D22
#define RelayPin3 21  //D21
#define RelayPin4 19  //D19
#define RelayPin5 18  //D18
#define RelayPin6 5   //D5
#define RelayPin7 25  //D25
#define RelayPin8 26  //D26

#define RelayPin9 13  //D13
#define RelayPin10 12  //D12
#define RelayPin11 14  //D14
#define RelayPin12 27  //D27
#define RelayPin13 33  //D33
#define RelayPin14 32  //D32
#define RelayPin15 15  //D15
#define RelayPin16 4   //D4

#define wifiLed    2   //D2
#define IR_RECV_PIN 35  //D35  

// WiFi Credentials
const char* ssid = "groundFloor";
const char* password = "Ground@Floor$12";

// device names
String Device_1_Name = "Room One Light";
String Device_2_Name = "Room One Fan";

String Device_3_Name = "Room Two Light";
String Device_4_Name = "Room Two Fan";

String Device_5_Name = "Room Three Light";
String Device_6_Name = "Room Three Fan";
String Device_7_Name = "Room Three Night Lamp";
String Device_8_Name = "Main Door Lock";

String Device_9_Name = "Kitchen Light";
String Device_10_Name = "Dinning Light";

String Device_11_Name = "Hall Light";
String Device_12_Name = "Hall Fan";
String Device_13_Name = "Gallery Light";
String Device_14_Name = "Hall Night Lamp";
String Device_15_Name = "AC";
String Device_16_Name = "Cooler";

int toggleState_1 = 0; //Define integer to remember the toggle state for relay 1
int toggleState_2 = 0; //Define integer to remember the toggle state for relay 2
int toggleState_3 = 0; //Define integer to remember the toggle state for relay 3
int toggleState_4 = 0; //Define integer to remember the toggle state for relay 4
int toggleState_5 = 0; //Define integer to remember the toggle state for relay 5
int toggleState_6 = 0; //Define integer to remember the toggle state for relay 6
int toggleState_7 = 0; //Define integer to remember the toggle state for relay 7
int toggleState_8 = 0; //Define integer to remember the toggle state for relay 8
int toggleState_9 = 0; //Define integer to remember the toggle state for relay 1
int toggleState_10 = 0; //Define integer to remember the toggle state for relay 2
int toggleState_11 = 0; //Define integer to remember the toggle state for relay 3
int toggleState_12 = 0; //Define integer to remember the toggle state for relay 4
int toggleState_13 = 0; //Define integer to remember the toggle state for relay 5
int toggleState_14 = 0; //Define integer to remember the toggle state for relay 6
int toggleState_15 = 0; //Define integer to remember the toggle state for relay 7
int toggleState_16 = 0; //Define integer to remember the toggle state for relay 8

IRrecv irrecv(IR_RECV_PIN);
decode_results results;

// prototypes
boolean connectWifi();

//callback functions
void firstLightChanged(uint8_t brightness);
void secondLightChanged(uint8_t brightness);
void thirdLightChanged(uint8_t brightness);
void fourthLightChanged(uint8_t brightness);
void fifthLightChanged(uint8_t brightness);
void sixthLightChanged(uint8_t brightness);
void senventhLightChanged(uint8_t brightness);
void eighthLightChanged(uint8_t brightness);
void ninthLightChanged(uint8_t brightness);
void tenthLightChanged(uint8_t brightness);
void eleventhLightChanged(uint8_t brightness);
void twelvethLightChanged(uint8_t brightness);
void thirteenthLightChanged(uint8_t brightness);
void fourteenthLightChanged(uint8_t brightness);
void fifteenthLightChanged(uint8_t brightness);
void sixteenthLightChanged(uint8_t brightness);





boolean wifiConnected = false;

//our callback functions
void firstLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
      digitalWrite(RelayPin1, LOW);
      Serial.println("Device1 ON");
      toggleState_1 = 1;
    }
  else
  {
    digitalWrite(RelayPin1, HIGH);
    Serial.println("Device1 OFF");
    toggleState_1 = 0;
  }
}

void secondLightChanged(uint8_t brightness)
{
  //Control the device 
  if (brightness == 255)
    {
      digitalWrite(RelayPin2, LOW);
      Serial.println("Device2 ON");
      toggleState_2 = 1;
    }
  else
  {
    digitalWrite(RelayPin2, HIGH);
    Serial.println("Device2 OFF");
    toggleState_2 = 0;
  }
}

void thirdLightChanged(uint8_t brightness)
{
  //Control the device  
  if (brightness == 255)
    {
      digitalWrite(RelayPin3, LOW);
      Serial.println("Device3 ON");
      toggleState_3 = 1;
    }
  else
  {
    digitalWrite(RelayPin3, HIGH);
    Serial.println("Device3 OFF");
    toggleState_3 = 0;
  }
}

void fourthLightChanged(uint8_t brightness)
{
  //Control the device 
  if (brightness == 255)
    {
      digitalWrite(RelayPin4, LOW);
      Serial.println("Device4 ON");
      toggleState_4 = 1;
    }
  else
  {
    digitalWrite(RelayPin4, HIGH);
    Serial.println("Device4 OFF");
    toggleState_4 = 0;
  }
}

void fifthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
      digitalWrite(RelayPin5, LOW);
      Serial.println("Device5 ON");
      toggleState_5 = 1;
    }
  else
  {
    digitalWrite(RelayPin5, HIGH);
    Serial.println("Device1 OFF");
    toggleState_5 = 0;
  }
}

void sixthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
      digitalWrite(RelayPin6, LOW);
      Serial.println("Device6 ON");
      toggleState_6 = 1;
    }
  else
  {
    digitalWrite(RelayPin6, HIGH);
    Serial.println("Device6 OFF");
    toggleState_6 = 0;
  }
}

void seventhLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
      digitalWrite(RelayPin7, LOW);
      Serial.println("Device7 ON");
      toggleState_7 = 1;
    }
  else
  {
    digitalWrite(RelayPin7, HIGH);
    Serial.println("Device7 OFF");
    toggleState_7 = 0;
  }
}

// servo motor for door lock
void eighthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
     

        s1.write(90);  
        delay(1000);  
      
      Serial.println("Device8 ON");
     toggleState_8 = 1;
    }
  else
  {
    
      s1.write(0);  
      delay(1000);  
    Serial.println("Device8 OFF");
    toggleState_8 = 1;
  }
}
void ninthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
      digitalWrite(RelayPin9, LOW);
      Serial.println("Device9 ON");
      toggleState_9 = 1;
    }
  else
  {
    digitalWrite(RelayPin9, HIGH);
    Serial.println("Device9 OFF");
    toggleState_9 = 0;
  }
}

void tenthLightChanged(uint8_t brightness)
{
  //Control the device 
  if (brightness == 255)
    {
      digitalWrite(RelayPin10, LOW);
      Serial.println("Device10 ON");
      toggleState_10 = 1;
    }
  else
  {
    digitalWrite(RelayPin10, HIGH);
    Serial.println("Device10 OFF");
    toggleState_10 = 0;
  }
}

void eleventhLightChanged(uint8_t brightness)
{
  //Control the device  
  if (brightness == 255)
    {
      digitalWrite(RelayPin11, LOW);
      Serial.println("Device11 ON");
      toggleState_11 = 1;
    }
  else
  {
    digitalWrite(RelayPin11, HIGH);
    Serial.println("Device11 OFF");
    toggleState_11 = 0;
  }
}

void twelvethLightChanged(uint8_t brightness)
{
  //Control the device 
  if (brightness == 255)
    {
      digitalWrite(RelayPin12, LOW);
      Serial.println("Device12 ON");
      toggleState_12 = 1;
    }
  else
  {
    digitalWrite(RelayPin12, HIGH);
    Serial.println("Device12 OFF");
    toggleState_12 = 0;
  }
}

void thirteenthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
      digitalWrite(RelayPin13, LOW);
      Serial.println("Device13 ON");
      toggleState_13 = 1;
    }
  else
  {
    digitalWrite(RelayPin13, HIGH);
    Serial.println("Device13 OFF");
    toggleState_13 = 0;
  }
}

void fourteenthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
      digitalWrite(RelayPin14, LOW);
      Serial.println("Device14 ON");
      toggleState_14 = 1;
    }
  else
  {
    digitalWrite(RelayPin14, HIGH);
    Serial.println("Device14 OFF");
    toggleState_14 = 0;
  }
}

void fifteenthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
      digitalWrite(RelayPin15, LOW);
      Serial.println("Device15 ON");
      toggleState_15 = 1;
    }
  else
  {
    digitalWrite(RelayPin15, HIGH);
    Serial.println("Device15 OFF");
    toggleState_15 = 0;
  }
}

void sixteenthLightChanged(uint8_t brightness)
{
  //Control the device
  if (brightness == 255)
    {
      digitalWrite(RelayPin15, LOW);
      Serial.println("Device16 ON");
      toggleState_16 = 1;
    }
  else
  {
    digitalWrite(RelayPin16, HIGH);
    Serial.println("Device16 OFF");
    toggleState_16 = 0;
  }
}

void relayOnOff(int relay){
    EspalexaDevice* d = espalexa.getDevice(relay-1); //the index is zero-based
    switch(relay){
      case 1: 
             if(toggleState_1 == 0){
              d->setPercent(100); //set value "brightness" in percent
              digitalWrite(RelayPin1, LOW); // turn on relay 1
              toggleState_1 = 1;
              Serial.println("Device1 ON");
              }
             else{
              d->setPercent(0); //set value "brightness" in percent
              digitalWrite(RelayPin1, HIGH); // turn off relay 1
              toggleState_1 = 0;
              Serial.println("Device1 OFF");
              }
      break;
      case 2: 
             if(toggleState_2 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin2, LOW); // turn on relay 2
              toggleState_2 = 1;
              Serial.println("Device2 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin2, HIGH); // turn off relay 2
              toggleState_2 = 0;
              Serial.println("Device2 OFF");
              }
      break;
      case 3: 
             if(toggleState_3 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin3, LOW); // turn on relay 3
              toggleState_3 = 1;
              Serial.println("Device3 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin3, HIGH); // turn off relay 3
              toggleState_3 = 0;
              Serial.println("Device3 OFF");
              }
      break;
      case 4: 
             if(toggleState_4 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin4, LOW); // turn on relay 4
              toggleState_4 = 1;
              Serial.println("Device4 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin4, HIGH); // turn off relay 4
              toggleState_4 = 0;
              Serial.println("Device4 OFF");
              }
      break;
      case 5: 
             if(toggleState_5 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin5, LOW); // turn on relay 5
              toggleState_5 = 1;
              Serial.println("Device5 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin5, HIGH); // turn off relay 5
              toggleState_5 = 0;
              Serial.println("Device5 OFF");
              }
      break;
      case 6: 
             if(toggleState_6 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin6, LOW); // turn on relay 6
              toggleState_6 = 1;
              Serial.println("Device6 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin6, HIGH); // turn off relay 6
              toggleState_6 = 0;
              Serial.println("Device6 OFF");
              }
      break;
      case 7: 
             if(toggleState_7 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin7, LOW); // turn on relay 7
              toggleState_7 = 1;
              Serial.println("Device7 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin7, HIGH); // turn off relay 7
              toggleState_7 = 0;
              Serial.println("Device7 OFF");
              }
      break;
      case 8: 
             if(toggleState_8 == 0){
              d->setPercent(100);
              s1.write(90);  
              delay(1000);  
             
              toggleState_8 = 1;
              Serial.println("Device8 ON");
              }
             else{
              d->setPercent(0);
               s1.write(0);  
              delay(1000);
              toggleState_8 = 0;
              Serial.println("Device8 OFF");
              }
      break;
            case 9: 
             if(toggleState_9 == 0){
              d->setPercent(100); //set value "brightness" in percent
              digitalWrite(RelayPin9, LOW); // turn on relay 9
              toggleState_9 = 1;
              Serial.println("Device9 ON");
              }
             else{
              d->setPercent(0); //set value "brightness" in percent
              digitalWrite(RelayPin9, HIGH); // turn off relay 9
              toggleState_9 = 0;
              Serial.println("Device9 OFF");
              }
      break;
      case 10: 
             if(toggleState_10 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin10, LOW); // turn on relay 10
              toggleState_10 = 1;
              Serial.println("Device10 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin10, HIGH); // turn off relay 10
              toggleState_10 = 0;
              Serial.println("Device10 OFF");
              }
      break;
      case 11: 
             if(toggleState_11 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin11, LOW); // turn on relay 11
              toggleState_11 = 1;
              Serial.println("Device11 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin11, HIGH); // turn off relay 11
              toggleState_11 = 0;
              Serial.println("Device11 OFF");
              }
      break;
      case 12: 
             if(toggleState_12 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin12, LOW); // turn on relay 4
              toggleState_12 = 1;
              Serial.println("Device12 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin12, HIGH); // turn off relay 4
              toggleState_12 = 0;
              Serial.println("Device12 OFF");
              }
      break;
      case 13: 
             if(toggleState_13 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin13, LOW); // turn on relay 5
              toggleState_13 = 1;
              Serial.println("Device5 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin13, HIGH); // turn off relay 5
              toggleState_13 = 0;
              Serial.println("Device13 OFF");
              }
      break;
      case 14: 
             if(toggleState_14 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin14, LOW); // turn on relay 6
              toggleState_14 = 1;
              Serial.println("Device14 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin14, HIGH); // turn off relay 6
              toggleState_14 = 0;
              Serial.println("Device14 OFF");
              }
      break;
      case 15: 
             if(toggleState_15 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin15, LOW); // turn on relay 7
              toggleState_15 = 1;
              Serial.println("Device15 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin15, HIGH); // turn off relay 7
              toggleState_15 = 0;
              Serial.println("Device15 OFF");
              }
      break;
      case 16: 
             if(toggleState_16 == 0){
              d->setPercent(100);
              digitalWrite(RelayPin16, LOW); // turn on relay 8
              toggleState_16 = 1;
              Serial.println("Device16 ON");
              }
             else{
              d->setPercent(0);
              digitalWrite(RelayPin16, HIGH); // turn off relay 8
              toggleState_16 = 0;
              Serial.println("Device16 OFF");
              }
      break;
      default : break;      
      }
      delay(300);  
}

// connect to wifi – returns true if successful or false if not
boolean connectWifi()
{
  boolean state = true;
  int i = 0;

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.println("Connecting to WiFi");

  // Wait for connection
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (i > 20) {
      state = false; break;
    }
    i++;
  }
  Serial.println("");
  if (state) {
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.println("Connection failed.");
  }
  return state;
}

void addDevices(){
  // Define your devices here.
  espalexa.addDevice(Device_1_Name, firstLightChanged); //simplest definition, default state off
  espalexa.addDevice(Device_2_Name, secondLightChanged);
  espalexa.addDevice(Device_3_Name, thirdLightChanged);
  espalexa.addDevice(Device_4_Name, fourthLightChanged);
  espalexa.addDevice(Device_5_Name, fifthLightChanged); 
  espalexa.addDevice(Device_6_Name, sixthLightChanged);
  espalexa.addDevice(Device_7_Name, seventhLightChanged);
  espalexa.addDevice(Device_8_Name, eighthLightChanged);
  espalexa.addDevice(Device_9_Name, firstLightChanged); 
  espalexa.addDevice(Device_10_Name, tenthLightChanged);
  espalexa.addDevice(Device_11_Name, eleventhLightChanged);
  espalexa.addDevice(Device_12_Name, twelvethLightChanged);
  espalexa.addDevice(Device_13_Name, thirteenthLightChanged); 
  espalexa.addDevice(Device_14_Name, fourteenthLightChanged);
  espalexa.addDevice(Device_15_Name, fifteenthLightChanged);
  espalexa.addDevice(Device_16_Name, sixteenthLightChanged);

  espalexa.begin();
}

void ir_remote(){
  if (irrecv.decode(&results)) {
    
      switch(results.value){
          case 0x80BF49B6:  relayOnOff(1);  break;
          case 0x80BFC936:  relayOnOff(2);  break;
          case 0x80BF33CC:  relayOnOff(3);  break;
          case 0x80BF718E:  relayOnOff(4);  break;
          case 0x80BFF10E:  relayOnOff(5);  break;
          case 0x80BF13EC:  relayOnOff(6);  break;
          case 0x80BF51AE:  relayOnOff(7);  break;
          case 0x80BFD12E:  relayOnOff(8);  break;
          case 0x80BF49B6:  relayOnOff(9);  break;
          case 0x80BFC936:  relayOnOff(10);  break;
          case 0x80BF33CC:  relayOnOff(11);  break;
          case 0x80BF718E:  relayOnOff(12);  break;
          case 0x80BFF10E:  relayOnOff(13);  break;
          case 0x80BF13EC:  relayOnOff(14);  break;
          case 0x80BF51AE:  relayOnOff(15);  break;
          case 0x80BFD12E:  relayOnOff(16);  break;
          default : break;         
        }   
        //Serial.println(results.value, HEX);    
        irrecv.resume();   
  } 
}

void setup()
{
  Serial.begin(115200);
  irrecv.enableIRIn(); // Start the receiver
   s1.attach(RelayPin8); 

  pinMode(RelayPin1, OUTPUT);
  pinMode(RelayPin2, OUTPUT);
  pinMode(RelayPin3, OUTPUT);
  pinMode(RelayPin4, OUTPUT);
  pinMode(RelayPin5, OUTPUT);
  pinMode(RelayPin6, OUTPUT);
  pinMode(RelayPin7, OUTPUT);
  pinMode(RelayPin9, OUTPUT);
  pinMode(RelayPin10, OUTPUT);
  pinMode(RelayPin11, OUTPUT);
  pinMode(RelayPin12, OUTPUT);
  pinMode(RelayPin13, OUTPUT);
  pinMode(RelayPin14, OUTPUT);
  pinMode(RelayPin15, OUTPUT);
  pinMode(RelayPin16, OUTPUT);


  pinMode(wifiLed, OUTPUT);


  //During Starting all Relays should TURN OFF
  digitalWrite(RelayPin1, HIGH);
  digitalWrite(RelayPin2, HIGH);
  digitalWrite(RelayPin3, HIGH);
  digitalWrite(RelayPin4, HIGH);
  digitalWrite(RelayPin5, HIGH);
  digitalWrite(RelayPin6, HIGH);
  digitalWrite(RelayPin7, HIGH);
  
  digitalWrite(RelayPin9, HIGH);
  digitalWrite(RelayPin10, HIGH);
  digitalWrite(RelayPin11, HIGH);
  digitalWrite(RelayPin12, HIGH);
  digitalWrite(RelayPin13, HIGH);
  digitalWrite(RelayPin14, HIGH);
  digitalWrite(RelayPin15, HIGH);
  digitalWrite(RelayPin16, HIGH);

  // Initialise wifi connection
  wifiConnected = connectWifi();

  if (wifiConnected)
  {
    addDevices();
  }
  else
  {
    Serial.println("Cannot connect to WiFi. So in Manual Mode");
    delay(1000);
  }
}

void loop()
{
  ir_remote();
  if (WiFi.status() != WL_CONNECTED)
  {
    //Serial.print("WiFi Not Connected ");
    digitalWrite(wifiLed, LOW); //Turn off WiFi LED
  }
  else
  {
    //Serial.print("WiFi Connected  ");
    digitalWrite(wifiLed, HIGH);
    //WiFi Control
    if (wifiConnected){
      espalexa.loop();
      delay(1);
    }
    else {
      wifiConnected = connectWifi(); // Initialise wifi connection
      if(wifiConnected){
      addDevices();
      }
    }
  }  

}
